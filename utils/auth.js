import jwtDecode from 'jwt-decode'

export const authenticateToken = (
    { store, $config, ...context },
    redirectTo
) => {
    const token = context.$cookies.get('_ttoauth')
    if (!token) {
        context.redirect(
            `${$config.ERP_SERVER}/login?redirect_url=${$config.CLIENT}${redirectTo}`
        )
        return false
    } else {
        const { full_name } = jwtDecode(token)
        // token not expire
        if (checkExpireToken(token)) {
            store.dispatch('getAdminInfo', { username: full_name, token })
            return true
        } else {
            context.$cookies.remove('_ttoauth', {
                domain: 'tuoitre.vn',
            })
            context.redirect(
                `${$config.ERP_SERVER}/login?redirect_url=${$config.CLIENT}${redirectTo}`
            )
            store.dispatch('logout')
            return false
        }
    }
}

export const checkRole = (listRole, paths) => {
    for (const path of paths) {
        if (listRole.includes(path)) return true
    }
    return false
}

export function checkExpireToken(token) {
    if(!token) return
    const { exp } = jwtDecode(token)
    return exp >= Date.now() / 1000
}
