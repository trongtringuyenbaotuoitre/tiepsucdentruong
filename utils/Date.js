export const formatDate = (date, type = "/") => {
  return `${date.getDate() > 9 ? date.getDate() : "0" + date.getDate()}${type}${
    date.getMonth() + 1 > 9 ? date.getMonth() + 1 : "0" + (date.getMonth() + 1)
  }${type}${date.getFullYear()}`;
};
export const getDateBefore = (amount) => {
  const listDateFormat = [];
  for (let i = 0; i < amount; i++) {
    listDateFormat.push(
      formatDate(new Date(Date.now() - i * 24 * 60 * 60 * 1000))
    );
  }
  return listDateFormat;
};
