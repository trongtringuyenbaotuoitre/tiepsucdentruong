const studentRawFormData = {
  0: {
    ten: "",
    ho: "",
    gioiTinh: "",
    ngayThangNamSinh: "",
    diaChi: "",
    tinhThanhPho: "",
    quanHuyen: "",
    phuongXa: "",
    email: "",
    soDienThoai: "",
    tenTruongCap3: "",
    soCMND: "",
    ngayCap: "",
    noiCap: "",
    matTruocCMND: [],
    matSauCMND: [],
    diemLop10: "",
    diemLop11: "",
    diemLop12: "",
    diemMon1: "",
    diemMon2: "",
    diemMon3: "",
    tongDiem: "",
    thanhTichHocTap: "",
    hocBong: "",
    nganhHocDaiHoc: "",
    truongDaiHoc: "",
    hinhThuc: "",
    giayBao: [],
    ghiChu: "",
  },
  1: {
    gioiThieuGiaDinh: "",
    lyDoCanHocBong: "",
    giayXacNhan: [],
  },
  2: {
    gioiThieuBanThan: "",
    cacHoatDong: "",
    vuotQuaThachThuc: "",
    dinhHuongTuongLai: "",
    taiSaoDuocHocBong: "",
    cauHoi: "",
  },
  3: {
    dapDenTiepNoi: "",
    hoTroNgoaiHocBong: "",
    boSungThongTin: "",
  },
};
const presenterRawFormData = {
  0: {
    tenNguoiGioiThieu: "",
    ho: "",
    diaChi: "",
    tinhThanhPho: "",
    quanHuyen: "",
    phuongXa: "",
    email: "",
    soDienThoai: "",
    ngheNghiep: "",
    moiQuanHe: "",
  },
  1: {
    tenSinhVien: "",
    hoSinhVien: "",
    diaChiSinhVien: "",
    tinhThanhPhoSinhVien: "",
    quanHuyenSinhVien: "",
    phuongXaSinhVien: "",
    emailSinhVien: "",
    soDienThoaiSinhVien: "",
    nganhHocDaiHoc: "",
    truongDaiHoc: "",
    phuongThucTrungTuyen: "",
    giayBao: [],
    matTruocCMND: [],
    matSauCMND: [],
    hoanCanhGiaDinh: "",
    noLucCaNhan: "",
    ghiChu: "",
  },
  2: {
    thongTinKhac: "",
  },
};

export default {
  "tan-sinh-vien": {
    formData: mapFormData(studentRawFormData),
    rawFormData: studentRawFormData,
  },
  "nguoi-gioi-thieu": {
    formData: mapFormData(presenterRawFormData),
    rawFormData: presenterRawFormData,
  },
  "tan-sinh-vien-dev": {
    formData: mapFormData(studentRawFormData),
    rawFormData: studentRawFormData,
  },
  "nguoi-gioi-thieu-dev": {
    formData: mapFormData(presenterRawFormData),
    rawFormData: presenterRawFormData,
  },
};

function mapFormData(rawFormData) {
  let result = {};
  for (let key in rawFormData) {
    Object.assign(result, rawFormData[key]);
  }
  return result;
}
