import { h } from "vue";

export const studentTableColumns = [
  {
    title: "Họ và tên",
    dataIndex: "name",
    key: "name",
    scopedSlots: { customRender: "user" },
    width: 300,
    sorter: true,
    customRender: (text, record) => {
      return h(
        "nuxt-link",
        {
          attrs: {
            to: `/cms/scholarship-detail/student?q=${record.id}`,
          },
        },
        [record.name]
      );
    },
  },
  {
    title: "Giới tính",
    dataIndex: "sex",
    key: "sex",
    width: 300,
    filters: [
      { text: "Nam", value: "nam" },
      { text: "Nữ", value: "nu" },
      { text: "Khác", value: "khac" },
    ],
    filterMultiple: false,
  },
  {
    title: "Ngày đăng ký",
    dataIndex: "date",
    key: "date",
    ellipsis: true,
    width: 300,
    sorter: true,
  },
  {
    title: "Địa chỉ",
    dataIndex: "address",
    key: "address",
    ellipsis: true,
  },
];

export const presenterTableColumns = [
  {
    title: "Họ và tên người giới thiệu",
    dataIndex: "name",
    key: "name",
    scopedSlots: { customRender: "user" },
    width: 300,
    sorter: true,
    customRender: (text, record) => {
      return h(
        "nuxt-link",
        {
          attrs: {
            to: `/cms/scholarship-detail/presenter?q=${record.id}`,
          },
        },
        [record.name]
      );
    },
  },
  {
    title: "Họ và tên sinh viên",
    dataIndex: "studentName",
    key: "studentName",
    width: 300,
  },
  {
    title: "Ngày đăng ký",
    dataIndex: "date",
    key: "date",
    ellipsis: true,
    width: 300,
    sorter: true,
  },
  {
    title: "Địa chỉ",
    dataIndex: "address",
    key: "address",
    ellipsis: true,
  },
];

export const positionTableColumns = [
  {
    title: "Id",
    dataIndex: "id",
    key: "id",
    width: 150,
  },
  {
    title: "Chức vụ",
    dataIndex: "pos_name",
    key: "pos_name",
    customRender: (text, record) => {
      return h(
        "nuxt-link",
        {
          attrs: {
            to: `/cms/assignment/${record.id}`,
          },
        },
        [record.pos_name]
      );
    },
  },
  {
    title: "Phòng",
    dataIndex: "dep_name",
    key: "dep_name",
  },
  {
    title: "Action",
    dataIndex: "",
    key: "x",
    scopedSlots: { customRender: "action" },
  },
];
