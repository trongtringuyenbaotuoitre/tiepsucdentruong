const validateEmail = (value = "", length) => {
  let msg = "";
  let regex = /^.+@.+\..+$/;
  let isEmail = regex.test(value);
  if (value.length === 0) {
    msg = "Email không được để trống!";
  } else if (value.length < length) {
    msg = `Email không được ist hơn ${length} kí tự!`;
  } else if (isEmail === false) {
    msg = "Email không đúng định dạng!";
  } else {
    msg = "";
  }
  return msg;
};
//params is password and length of password
const validateMaxLength = (value = 0, length) => {
  let msg = "";
  if (!value) {
    msg = `Trường này không được để trống!`;
  } else if (value.length > length) {
    msg = `Trường này không được nhiều hơn ${length} kí tự!`;
  } else {
    msg = "";
  }
  return msg;
};
const validateMinLength = (value = 0, length) => {
  let msg = "";
  if (Array.isArray(value)) {
    if (value.length === 0) {
      msg = `Trường này không được để trống!`;
    }
  } else if (!value) {
    msg = `Trường này không được để trống!`;
  } else if (value.length < length) {
    msg = `Trường này không được ít hơn ${length} kí tự!`;
  } else {
    msg = "";
  }
  return msg;
};

//params phone
const validatePhone = (phone) => {
  let msg = "";
  // let regex = /^(\+[\d]{11,12}|0[\d]{9})$/;
  const regexNumber = /^[0-9]*$/;

  let isPhone = regexNumber.test(phone);
  if (phone.length === 0) {
    msg = "Số điện thoại không được để trống!";
  } else if (isPhone === false) {
    msg = `Số điện thoại không hợp lệ`;
  } else if (phone.length < 10) {
    msg = `Số điện thoại phải có 10 số `;
  } else if (phone.length > 10) {
    msg = `Số điện thoại chỉ có 10 số `;
  } else {
    msg = "";
  }
  return msg;
};

//params value of field and field name to validate
const validateRequired = (value = "", name = "", length = 0, max = 1000) => {
  let msg = "";
  if (value.length === 0) {
    msg = name + " không được để trống!";
  } else if (value.length < length) {
    msg = `${name} không được ít hơn ${length} kí tự!`;
  } else if (value.length > max) {
    msg = `${name} không được quá ${max} kí tự!`;
  } else {
    msg = "";
  }
  return msg;
};

const validateLength = (value, length, label) => {
  if (value.length !== length) {
    return `${label || "Trường này"} không đúng định dạng!`;
  }
  return "";
};

const requireLength = (value, requireValue, label) => {
  if (!requireValue.includes(value.length)) {
    return `${label || "Trường này"} không hợp lệ!`;
  }
};

const validateStringOnlyNumber = (value, param, label) => {
  const regex = /\d+$/;
  if (!regex.test(value)) {
    return `${label || "Trường này"} không hợp lệ!`;
  }
  return "";
};

const validateRangeValue = (value, range, label) => {
  const messageValidate = validateStringOnlyNumber(value);
  if (messageValidate) return messageValidate;

  const [min, max] = range;
  if (value < min) {
    return `${"Trường này"} không được nhỏ hơn ${min} `;
  } else if (value > max) {
    return `${"Trường này"} không được lớn hơn ${max} `;
  }
  return "";
};

const validateRangeLength = (value, range, label) => {
  const [minLength, maxLength] = range;
  if (value.length < minLength || value.length > maxLength) {
    return `${
      label || "Trường này"
    } chỉ có ${minLength} hoặc ${maxLength} kí tự`;
  }
  return "";
};

const exactLength = (value, listLength = [], label) => {
  if (!listLength.includes(value.length)) {
    let msg = `${label || "Trường này"} chỉ có `;
    listLength.forEach((length, index) => {
      if (index === 0) {
        msg += length;
      } else if (index === listLength.length - 1) {
        msg += ` hoặc ${length} kí tự`;
      } else {
        msg += ` hoặc ${length}`;
      }
    });
    return msg;
  }
  return "";
};

const maxAmountWords = (value = "", maxCount) => {
  const arr = value.split(" ");

  return arr.filter((word) => word !== "").length < maxCount
    ? ""
    : `Trường này không được quá ${maxCount} kí tự!`;
};
export default {
  required: validateRequired,
  minLength: validateMinLength,
  maxLength: validateMaxLength,
  email: validateEmail,
  phone: validatePhone,
  length: validateLength,
  requireLength: requireLength,
  number: validateStringOnlyNumber,
  range: validateRangeValue,
  rangeLength: validateRangeLength,
  exactLength: exactLength,
  maxAmountWords: maxAmountWords,
};
