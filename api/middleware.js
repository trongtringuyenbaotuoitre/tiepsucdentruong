import { checkExpireToken } from "../utils/auth";

export const validateRequest = (req, res, next) => {
  const token = req.headers.authorization;
  if (!token || !checkExpireToken(token)) {
    res.status(401).json({
      message: "Phiên làm việc hết hạn, vui lòng tải lại trang",
    });
  } else {
    next();
  }
};
