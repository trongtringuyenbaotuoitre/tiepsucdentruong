import addressRouter from './Routes/address.route'
import schoolarshipRouter from './Routes/scholarship.route'
import cmsRouter from './Routes/cms.route'
import cors from 'cors'
import nuxtConfig from '../nuxt.config'

var corsOptions = {
    origin: nuxtConfig.publicRuntimeConfig.CLIENT,
    optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
}
const express = require('express')
const app = express()

app.use(cors(corsOptions))

//json decode
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use('/address', addressRouter)
app.use('/schoolarship', schoolarshipRouter)
app.use('/cms', cmsRouter)

module.exports = {
    path: '/api/',
    handler: app,
}
