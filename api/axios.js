import axios from 'axios'
import nuxtConfig from '../nuxt.config'

export const AxiosExpress = axios.create()
export const AxiosConnectBackend = axios.create({
    baseURL: nuxtConfig.publicRuntimeConfig.SERVER,
})
