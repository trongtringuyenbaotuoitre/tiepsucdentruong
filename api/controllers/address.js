import { AxiosExpress } from "../axios";

export const ListProvinces = async (req, res) => {
  try {
    const response = await AxiosExpress.get(
      "https://provinces.open-api.vn/api/p"
    );
    const data = response.data;
    res.json({ provinces: data });
  } catch (e) {
    res.status(500).end();
  }
};
export const ListDistrict = async (req, res) => {
  const { provinceCode } = req.params;
  try {
    const response = await AxiosExpress.get(
      `https://provinces.open-api.vn/api/p/${provinceCode}?depth=2`
    );
    const { districts } = response.data;

    res.json({ districts });
  } catch ({ response }) {
    res.status(500).end();
  }
};

export const ListWard = async (req, res) => {
  const { districtCode } = req.params;
  try {
    const response = await AxiosExpress.get(
      `https://provinces.open-api.vn/api/d/${districtCode}?depth=2`
    );
    const { wards } = response.data;

    res.json({ wards });
  } catch ({ response }) {
    res.status(500).end();
  }
};
