import { generateURL } from '../../utils/mapping'
import { AxiosConnectBackend } from '../axios'
const multer = require('multer')
const fs = require('fs')

const fileStorageEngine = multer.diskStorage({
    destination: (req, file, cb) => {
        fs.mkdir('./uploads/', err => {
            cb(null, './uploads/')
        })
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + '--' + file.originalname)
    },
})

export const upload = multer({ storage: fileStorageEngine })

export const createSchoolarshipRequest = async (req, res) => {
    const { type } = req.params
    const formData = req.body
    const files = req.files

    for (let key in files) {
        formData[key] = files[key][0].path
    }

    try {
        const response = await AxiosConnectBackend.post(
            `/information/${
                type === 'tan-sinh-vien' ? 'student' : 'presenter'
            }`,
            formData
        )
        if (response.status === 200) {
            res.json({ message: 'Đăng ký thành công' })
        }
    } catch ({ response, ...error }) {
        console.log(response)
        res.status(response.status).json({
            message:
                'Đã có lỗi xảy ra, vui lòng chờ trong giây lát và thử lại sau',
        })
    }
}

export const getRegistrationStudent = async (req, res) => {
    const token = req.headers.authorization
    const URL = generateURL('/cms/get-student?', req.query)
    try {
        const response = await AxiosConnectBackend.get(encodeURI(URL), {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })

        res.json(response.data)
    } catch (error) {
        console.log('getRegistrationStudent scholarship.controller.js', error)
        res.status(500).json({
            message:
                'Đã có lỗi xảy ra, vui lòng chờ trong giây lát và thử lại sau',
        })
    }
}

export const getRegistrationPresenter = async (req, res) => {
    const token = req.headers.authorization
    const URL = generateURL('/cms/get-presenter?', req.query)
    try {
        const response = await AxiosConnectBackend.get(URL, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
        res.json(response.data)
    } catch (error) {
        console.log('getRegistrationPresenter scholarship.controller.js', error)
        res.status(500).json({
            message:
                'Đã có lỗi xảy ra, vui lòng chờ trong giây lát và thử lại sau',
        })
    }
}
