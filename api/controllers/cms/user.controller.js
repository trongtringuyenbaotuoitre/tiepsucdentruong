import { AxiosConnectBackend } from '../../axios'
import Downloader from 'nodejs-file-downloader'
import { mappingRoles } from '../../../utils/mapping'

export const getDetail = async (req, res) => {
    const token = req.headers.authorization
    const { type, id } = req.params
    if (type === 'student') {
        try {
            const response = await AxiosConnectBackend.get(
                `/cms/scholarship-detail/student/${id}`,
                {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                }
            )
            const { status, data, message } = response

            if (status === 200) {
                res.json(data)
            } else {
                res.status(status).json({ message })
            }
        } catch ({ response }) {
            console.log('getDetail user.controller.js', error)
            res.status(response.status).json({
                message:
                    'Đã có lỗi xảy ra, vui lòng chờ trong giây lát và thử lại sau',
            })
        }
    } else {
        try {
            const { status, data } = await AxiosConnectBackend.get(
                `/cms/scholarship-details/presenter/${id}`,
                {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                }
            )
            if (status === 200) {
                res.json(data)
            } else {
                res.status(status).json({ message: data })
            }
        } catch (error) {
            console.log('getDetail user.controller.js', error)
            res.status(response.status).json({
                message:
                    'Đã có lỗi xảy ra, vui lòng chờ trong giây lát và thử lại sau',
            })
        }
    }
}

export const downloadExcelFile = async (req, res) => {
    const token = req.headers.authorization
    const params = req.body.reduce((Arr, { name, checked }) => {
        if (checked) {
            Arr.push(name)
        }
        return Arr
    }, [])

    const fileName = `danh-sach-dang-ky-hoc-bong-${Date.now()}.xlsx`

    const downloader = new Downloader({
        url: `http://api.yii2.local/tx/cms/export-student?${params.join('&')}`,
        directory: './static/downloads',
        fileName: fileName,
        headers: {
            Authorization: `Bearer ${token}`,
        },
    })

    try {
        await downloader.download()
        res.json({ fileName })
    } catch (error) {
        console.log('Download failed', error.responseBody)
        res.status(500).end()
    }
}

export const getPosition = async (req, res) => {
    const token = req.headers.authorization
    try {
        const response = await AxiosConnectBackend.get('/cms/get-position', {
            headers: {
                authorization: `Bear ${token}`,
            },
        })
        res.json(response.data)
    } catch (error) {
        console.log(error)
        console.log('func getPosition user.controller.js')
        res.status(500).end()
    }
}

export const getPositionRoles = async (req, res) => {
    const token = req.headers.authorization
    const { positionId } = req.params
    try {
        const response = await AxiosConnectBackend.get(
            `/cms/get-action-position?id=${positionId}`,
            {
                headers: {
                    authorization: `Bear ${token}`,
                },
            }
        )
        const { pos_name, notAssignRoles, assignRoles } = response.data
        res.json({
            title: pos_name,
            notAssignRoles: mappingRoles(notAssignRoles),
            assignRoles: assignRoles.map(({ action_name }) => action_name),
        })
    } catch (error) {
        console.log('func getPositionRoles user.controller.js')
        res.status(500).end()
    }
}

export const savePositionRoles = async (req, res) => {
    const token = req.headers.authorization
    const { positionId } = req.params
    const { assignRoles } = req.body
    try {
        await AxiosConnectBackend.get(
            `/cms/save?id=${positionId}&${assignRoles.join('&')}`,
            {
                headers: {
                    authorization: `Bearer ${token}`,
                },
            }
        )
        res.end()
    } catch (error) {
        console.log(error)
        res.status(500).end()
    }
}

export const getListRole = async (req, res) => {
    const token = req.headers.authorization
    try {
        const response = await AxiosConnectBackend.get(`/cms/get-list-role`, {
            headers: {
                authorization: `Bearer ${token}`,
            },
        })
        res.json(response.data)
    } catch (error) {
        console.log(error)
        res.status(500).end()
    }
}

export const createNewPosition = async (req, res) => {
    const token = req.headers.authorization
    try {
        await AxiosConnectBackend.post('/cms/create-pos', req.body, {
            headers: {
                authorization: `Bearer ${token}`,
            },
        })
        res.end()
    } catch (error) {
        console.log(error)
        res.status(500).end()
    }
}

export const updatePosition = async (req, res) => {
    const token = req.headers.authorization
    const { id, ...formData } = req.body
    try {
        await AxiosConnectBackend.post(`/cms/update-pos?id=${id}`, formData, {
            headers: {
                authorization: `Bearer ${token}`,
            },
        })
        res.end()
    } catch (error) {
        console.log(error)
        res.status(500).end()
    }
}
