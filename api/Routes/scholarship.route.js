import { Router } from 'express'
const studentRouter = Router()
import {
    createSchoolarshipRequest,
    upload,
    getRegistrationStudent,
    getRegistrationPresenter,
} from '../controllers/scholarship.controller'
import { validateRequest } from '../middleware'

studentRouter.post(
    '/create-schoolarship-request/:type',
    validateRequest,
    upload.fields([
        {
            name: 'frontCMND',
            maxCount: 1,
        },
        {
            name: 'backCMND',
            maxCount: 1,
        },
        {
            name: 'certification',
            maxCount: 1,
        },
        {
            name: 'admissionNotice',
            maxCount: 1,
        },
    ]),

    createSchoolarshipRequest
)

studentRouter.get('/getStudent', validateRequest, getRegistrationStudent)
studentRouter.get('/getPresenter', validateRequest, getRegistrationPresenter)

export default studentRouter
