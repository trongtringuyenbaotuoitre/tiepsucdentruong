import { Router } from "express";
import { refreshToken } from "../controllers/cms/auth.controller";
import {
  getNumberOfRegistrationRecent7Days,
  getNumberOfRegistrationCities,
} from "../controllers/cms/DashBoard.controller";
import {
  getDetail,
  downloadExcelFile,
  getPosition,
  getPositionRoles,
  savePositionRoles,
  getListRole,
  createNewPosition,
  updatePosition,
} from "../controllers/cms/user.controller";
import { validateRequest } from "../middleware";

const cmsRouter = Router();

cmsRouter.get(
  "/getRecent",
  validateRequest,
  getNumberOfRegistrationRecent7Days
);
cmsRouter.get("/registrationCities", getNumberOfRegistrationCities);
cmsRouter.get("/detail/:type/:id", validateRequest, getDetail);
cmsRouter.post("/download", validateRequest, downloadExcelFile);
cmsRouter.get("/getNewToken", refreshToken);
cmsRouter.get("/getPosition", validateRequest, getPosition);
cmsRouter.get(
  "/getPositionRoles/:positionId",
  validateRequest,
  getPositionRoles
);
cmsRouter.post(
  "/savePositionRoles/:positionId",
  validateRequest,
  savePositionRoles
);
cmsRouter.get("/getListRole", validateRequest, getListRole);
cmsRouter.post("/create-pos", validateRequest, createNewPosition);
cmsRouter.post("/update-pos", validateRequest, updatePosition);
export default cmsRouter;
