import Vue from "vue";
import VueEasyLightbox from "vue-easy-lightbox";
import "vue-easy-lightbox/external-css/vue-easy-lightbox.css";

Vue.use(VueEasyLightbox);
