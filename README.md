# Document

## Thêm yêu cầu nhận học bổng

### Sinh viên:

- URL: **${domain}/student**
- Method: **POST**
- Body: **Object**

| Field                  | Data type |
| ---------------------- | --------- |
| frontCMND              | String    |
| backCMND               | String    |
| certification          | String    |
| admissionNotice        | String    |
| name                   | String    |
| surname                | String    |
| sex                    | String    |
| "date"OfBirth          | String    |
| "address"              | String    |
| city                   | String    |
| district               | String    |
| village                | String    |
| email                  | String    |
| phone                  | String    |
| highSchoolName         | String    |
| score10                | String    |
| score11                | String    |
| score12                | String    |
| scoreSubject1          | String    |
| scoreSubject2          | String    |
| scoreSubject3          | String    |
| total                  | String    |
| academicAchievement    | String    |
| scholarship            | String    |
| major                  | String    |
| university             | String    |
| matriculation          | String    |
| note                   | String    |
| numberCMND             | String    |
| "date"                 | String    |
| issued_by              | String    |
| introduce              | String    |
| activity               | String    |
| overcomeChallenge      | String    |
| futureOrientation      | String    |
| whyNeed                | String    |
| question               | String    |
| introduceFamily        | String    |
| reason                 | String    |
| reciprocate            | String    |
| moreSupportScholarship | String    |
| moreInformation        | String    |

### Người giới thiệu:

- URL: **${domain}/presenter**
- Method: **POST**
- Body: **Object**

| Field              | Data type |
| ------------------ | --------- |
| frontCMND          | String    |
| backCMND           | String    |
| admissionNotice    | String    |
| namePresenter      | String    |
| surname            | String    |
| "address"          | String    |
| city               | String    |
| email              | String    |
| phone              | String    |
| job                | String    |
| relationship       | String    |
| note               | String    |
| namePresenter      | String    |
| surnamePresenter   | String    |
| "address"Presentee | String    |
| cityPresentee      | String    |
| districtPresentee  | String    |
| villagePresentee   | String    |
| emailPresentee     | String    |
| phonePresentee     | String    |
| major              | String    |
| university         | String    |
| method             | String    |
| situation          | String    |
| effort             | String    |
| description        | String    |

## Lấy danh sách sinh viên

- URL: **${domain}/cms/getStudents?page={numberOfPage}&limit={pageSize}**
- Method: **GET**
- Response:

| Status | Response data | Example | Description                                                                        |
| ------ | ------------- | ------- | ---------------------------------------------------------------------------------- |
| 200    | Object        |         | Total là tổng số phần tử trả về, mỗi một phần tử trong array là 1 dòng trong table |
| 401    | Object        |         | Token expired                                                                      |

- Example: **Status 200**

```json
{
  "total": 12,
  "data": [
    {
      "id": "1",
      "name": "Nguyễn Trọng Trí",
      "sex": "nam",
      "date": "16/09/2022",
      "address": "60A Hoàng Văn Thụ, phường 9, quận Phú Nhuận",
    },
    {
      "id": "2",
      "name": "Jim Green",
      "sex": "nam",
      "date": "16/09/2022",
      "address": "London No. 2 Lake Park, London No. 2 Lake Park",
    },
    {
      "id": "3",
      "name": "Joe Black",
      "sex": "nam",
      "date": "16/09/2022",
      "address": "Sidney No. 1 Lake Park, Sidney No. 1 Lake Park",
    },
    ...
  ]
}
```

- Example: **Status 401**

```json
{
  "message": "Unauthorized"
}
```

## Lấy danh sách người giới thiệu

- URL: **${domain}/cms/getPresenter?page={numberOfPage}&limit={pageSize}**
- Method: **GET**
- Response:

| Status | Response data | Example | Description                                                                        |
| ------ | ------------- | ------- | ---------------------------------------------------------------------------------- |
| 200    | Object        |         | Total là tổng số phần tử trả về, mỗi một phần tử trong array là 1 dòng trong table |
| 401    | Object        |         | Token expired                                                                      |

- Example: **Status 200**

```json
{
  "total": 12,
  "data": [
    {
      "id": "1",
      "name": "Nguyễn Trọng Trí",
      "studentName": "Nguyễn Trọng Trí",
      "date": "16/09/2022",
      "address": "60A Hoàng Văn Thụ, phường 9, quận Phú Nhuận"
    },
    {
      "id": "2",
      "name": "Jim Green",
      "studentName": "Nguyễn Trọng Trí",
      "date": "16/09/2022",
      "address": "London No. 2 Lake Park, London No. 2 Lake Park"
    },
    {
      "id": "3",
      "name": "Joe Black",
      "studentName": "Nguyễn Trọng Trí",
      "date": "16/09/2022",
      "address": "Sidney No. 1 Lake Park, Sidney No. 1 Lake Park"
    },
    ...
  ]
}
```

- Example: **Status 401**

```json
{
  "message": "Token expired"
}
```

## Thông tin chi tiết sinh viên đăng ký

- URL: **${domain}/cms/scholarship-detail/student/{id}**
- Method: **GET**
- Params:

  | Name | Example | Description      |
  | ---- | ------- | ---------------- |
  | id   | 1       | Id của sinh viên |

- Response:

| Status | Response data | Example | Description                                                                                                                                                          |
| ------ | ------------- | ------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 200    | Object        |         | tab-{i} là array chứa data của 1 tab với mỗi item là 1 dòng trong table. Trong đó, id là unique random string, field là label của dòng, content là nội dung của dòng |
| 400    | Object        |         | Trả về message nếu id không hợp lệ                                                                                                                                   |
| 401    | Object        |         | Unauthorized                                                                                                                                                         |

- Example: **Status 200**

```json
{
  "tab-1": [
    {
      "id": "1",
      "field": "Thông tin cơ bản",
      "content": [
        {
          "value": "Đinh Thị Trà My",
          "name": "name",
          "type": "string",
          "label": "Họ và tên"
        },

        {
          "value": "Nữ",
          "name": "sex",
          "type": "string",
          "label": "Giới tính"
        },
        {
          "value": "12/11/2003",
          "name": "dateOfBirth",
          "type": "string",
          "label": "Năm sinh"
        },
        {
          "value": "Ấp sậy giăng xã Vĩnh Trị huyện Vĩnh Hưng tỉnh Long An, Xã Vĩnh Trị, Huyện Vĩnh Hưng, Tỉnh Long An",
          "name": "address",
          "type": "string",
          "label": "Địa chỉ"
        },
        {
          "value": "dangthithuyhuyen135@gmail.com",
          "name": "email",
          "type": "string",
          "label": "Email"
        },

        {
          "value": "0961923130",
          "name": "phone",
          "type": "string",
          "label": "Số điện thoại"
        },
        {
          "value": "025303001701",
          "name": "numberCMND",
          "type": "string",
          "label": "Số CMND hoặc CCCD"
        },
        {
          "value": "27/03/2021",
          "name": "date",
          "type": "string",
          "label": "Ngày cấp"
        },
        {
          "value": "Công an tỉnh Phú Thọ",
          "name": "issued_by",
          "type": "string",
          "label": "Nơi cấp"
        },
        {
          "value": "/images/banner.jpeg",
          "name": "frontCMND",
          "type": "image",
          "label": "Mặt trước CMND hoặc CCCD"
        },
        {
          "value": "/images/banner.jpeg",
          "name": "backCMND",
          "type": "image",
          "label": "Mặt sau CMND hoặc CCCD"
        }
      ]
    },
    {
      "id": "2",
      "field": "Thông tin trường học",
      "content": [
        {
          "value": "THPT Thanh Sơn",
          "name": "highSchoolName",
          "type": "string",
          "label": "Tên trường cấp 3"
        }
      ]
    },
    {
      "id": "3",
      "field": "Thông tin học tập",
      "content": [
        {
          "value": "8.50",
          "name": "score10",
          "type": "string",
          "label": "Điểm trung bình lớp 10"
        },
        {
          "value": "8.50",
          "name": "score11",
          "type": "string",
          "label": "Điểm trung bình lớp 11"
        },
        {
          "value": "8.50",
          "name": "score12",
          "type": "string",
          "label": "Điểm trung bình lớp 12"
        }
      ]
    },
    {
      "id": "4",
      "field": "Thông tin đại học",
      "content": [
        {
          "value": "8.50",
          "name": "total",
          "type": "string",
          "label": "Tổng điểm 3 môn xét tuyển đại học"
        },
        {
          "value": "Sư phạm mầm non năm 2021",
          "name": "major",
          "type": "string",
          "label": "Đã đỗ vào ngành"
        },
        {
          "value": "Đại học Hùng Vương - Phú Thọ",
          "name": "university",
          "type": "string",
          "label": "Trường"
        },
        {
          "value": "Xét tuyển điểm thi THPT",
          "name": "matriculation",
          "type": "string",
          "label": "Hình thức trúng tuyển"
        },
        {
          "value": "Toán : 6,4 Sử : 8,5 Địa : 9,5",
          "name": "note",
          "type": "string",
          "label": "Ghi chú hình thức trúng tuyển"
        },
        {
          "value": "/images/banner.jpeg",
          "name": "admissionNotice",
          "type": "image",
          "label": "Giấy báo trúng tuyển"
        }
      ]
    },
    {
      "id": "5",
      "field": "Thông tin khác",
      "content": [
        {
          "value": "Tham dự kỳ thi học sinh giỏi cấp tỉnh năm học 2020-2021",
          "name": "academicAchievement",
          "type": "string",
          "label": "Thành tích học tập khác"
        },
        {
          "value": "Học bổng của trường Đại học Hùng Vương",
          "name": "scholarship",
          "type": "string",
          "label": "Đã từng được các học bổng"
        }
      ]
    }
  ],
  "tab-2": [
    {
      "id": "2",
      "field": "Giới thiệu về gia đình",
      "content": [
        {
          "value": "Tôi tên là Đinh Thị Trà My , sn 12/10/2003 . Quê quán : Khu 1 ,Cự Thắng - Thanh Sơn - Phú Thọ . Hiện tại đang là sinh viên năm 2 , nghành giáo dục mầm non của trường Đại học Hùng Vương - Phú Thọ . Tôi từ khi sinh ra và lớn lên chịu nhiều những thiệt thòi hơn các bạn cùng trang lứa , sống trong gia đình bố vũ phu , thường chuyên chứng kiến cảnh bố đánh đập mẹ cũng vì lí do đó mà phần nào khiến bản thân tôi bị ám ảnh . Khi tôi học lớp 10 , ba mẹ ly thân và vào năm lớp 11 ba mẹ ly hôn . 3 chị e tôi sống với nhau , 1 em sinh năm 2007 , 1 em sinh năm 2009 . Năm 2020 là khoảng thời gian khiến tôi gục ngã nhất : mẹ đi lấy chồng 2 và bố cũng đi lấy vợ . Cả bố và mẹ đều có cuộc sống riêng . Hiện tại ngoài những lúc ở trường thì tôi về nhà ông bà ngoại ở tại Khu 10 xã Tất Thắng , những tiền sinh hoạt hàng ngày là do tôi đi làm thêm và năm nhất vừa rồi được hỗ trợ của nhà nước và tiền học bổng của nhà trường . Từ khi ba mẹ tôi ly hôn thì ba mẹ không lo cho tôi .",
          "name": "introduceFamily",
          "type": "string",
          "label": ""
        }
      ]
    },
    {
      "id": "3",
      "field": "Thông tin học tập",
      "content": [
        {
          "value": "Lí do tôi cần học bổng này là vì tôi dùng để sinh hoạt hàng ngày , nộp các khoản tiền tại trường học",
          "name": "whyNeed",
          "type": "string",
          "label": ""
        }
      ]
    },
    {
      "id": "4",
      "field": "Giấy chứng nhận hộ nghèo hoặc giấy xác nhận hoàn cảnh đặc biệt khó khăn của UBND phường, xã",
      "content": [
        {
          "value": "/images/banner.jpeg",
          "name": "giayXacNhan",
          "type": "image",
          "label": ""
        }
      ]
    }
  ],
  "tab-3": [
    {
      "id": "1",
      "field": "Giới thiệu về bản thân",
      "content": [
        {
          "value": "Khi còn là học sinh ngồi trên ghế nhà trường tôi luôn nỗ lực phấn đấu học tập tốt . Đạt được danh hiệu học sinh khá , giỏi , học sinh giỏi cấp huyện , cấp tỉnh . Sau khi tốt nghiệp THPT , tháng 7,8 năm 2021 tôi đã xin việc làm may tại công ty may Sơn Hà - Thanh Thủy - Phú Thọ . Với số tiền của 2 tháng lương tôi vẫn quyết tâm đi học Đại học . Tháng 9 năm 2021 tôi là sinh viên của Trường Đại học Hùng Vương - Phú Thọ . Với sự cố gắng học tập , nỗ lực không ngừng của bản thân , hết học kỳ 1 năm học 2021 - 2022 tôi đã giành được học bổng của trường .",
          "name": "introduce",
          "type": "string",
          "label": ""
        }
      ]
    },
    {
      "id": "2",
      "field": "Các hoạt động tình nguyện, hỗ trợ cộng đồng",
      "content": [
        {
          "value": "Tham gia câu lạc bộ tình nguyện xanh của trường Đại Học Hùng Vương , tham gia tổ chức trung thu cho các em tại Trung Tâm bảo trợ trẻ mồ côi và tàn tật thành phố Việt Trì , Tham gia tiếp sức mùa thi , kêu gọi ,quyên góp hộ các em có hoàn cảnh khó khăn thuộc Mỹ Á huyện Tân Sơn",
          "name": "activity",
          "type": "string",
          "label": ""
        }
      ]
    },
    {
      "id": "3",
      "field": "Vượt qua thách thức",
      "content": [
        {
          "value": "Mình càng phải cố gắng hết sức mình khi ba mẹ quyết định ly hôn , luôn mạnh mẽ , không rung động trước những cám dỗ , không ăn chơi xa đọa . Biết bản thân mình ở đâu và như thế nào nên k đua đòi theo các bạn cùng trang lứa",
          "name": "overcomeChallenge",
          "type": "string",
          "label": ""
        }
      ]
    },
    {
      "id": "4",
      "field": "Định hướng tương lai",
      "content": [
        {
          "value": "Mình sẽ cố gắng học tập tốt , để trở thành 1 người giáo viên có phẩm đạo đức tốt , có kỹ năng , có kinh nghiệm yêu mến trẻ yêu nghề . Xây dựng quê hương góp phần phát triển đất nước ngày thêm đẹp giàu . Cố gắng nỗ lực hết mình để có cơ hội giúp đỡ các bạn khác cũng có hoàn cảnh giống như mình hoặc bất hạnh hơn mình . Để không ai bị bỏ lại phía sau .",
          "name": "futureOrientation",
          "type": "string",
          "label": ""
        }
      ]
    }
  ],
  "tab-4": [
    {
      "id": "1",
      "field": "Đáp đền tiếp nối",
      "content": [
        {
          "value": "Mình sẽ nỗ lực để sau này thành công sẽ giúp đỡ các bạn có hoàn cảnh giống mình để họ không bị bỏ lại phía sau , giúp họ có 1 tương lai tốt đẹp .",
          "name": "reciprocate",
          "type": "string",
          "label": ""
        }
      ]
    },
    {
      "id": "2",
      "field": "Hỗ trợ ngoài học bổng",
      "content": [
        {
          "value": "khong",
          "name": "moreSupportScholarship",
          "type": "string",
          "label": ""
        }
      ]
    },
    {
      "id": "3",
      "field": "Thông tin bổ sung thêm",
      "content": [
        {
          "value": "Mình là sinh viên lớp k19 Đại học mầm non , sinh viên năm 2 nghành sư phạm mầm non của trường Đại Học Hùng Vương",
          "name": "moreInformation",
          "type": "string",
          "label": ""
        }
      ]
    }
  ]
}
```

- Example: **Status 400**

```json
{
  "message": "Thông tin không hợp lệ"
}
```

- Example: **Status 401**

```json
{
  "message": "Unauthorized"
}
```

## Thông tin chi tiết người giới thiệu

- URL: **${domain}/cms/scholarship-detail/presenter/{id}**
- Method: **GET**
- Params:

  | Name | Example | Description             |
  | ---- | ------- | ----------------------- |
  | id   | 1       | Id của người giới thiệu |

- Response:

| Status | Response data | Example | Description                                                                                                                                                          |
| ------ | ------------- | ------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 200    | Object        |         | tab-{i} là array chứa data của 1 tab với mỗi item là 1 dòng trong table. Trong đó, id là unique random string, field là label của dòng, content là nội dung của dòng |
| 400    | Object        |         | Trả về message nếu id không hợp lệ                                                                                                                                   |
| 401    | Object        |         | Unauthorized                                                                                                                                                         |

- Example: **Status 200**

```json
{
  "tab-1": [
    {
      "id": "1",
      "field": "Họ và tên",
      "content": [
        {
          "value": "Nguyễn Thị Trinh",
          "name": "namePresenter",
          "type": "string",
          "label": ""
        }
      ]
    },
    {
      "id": "2",
      "field": "Địa chỉ",
      "content": [
        {
          "value": "767 trần hưng đạo, Phường Điện Ngọc, Thị xã Điện Bàn, Tỉnh Quảng Nam",
          "name": "address",
          "type": "string",
          "label": ""
        }
      ]
    },
    {
      "id": "3",
      "field": "Email",
      "content": [
        {
          "value": "nguyenthitrinh0935418514@gmail.com",
          "name": "address",
          "type": "string",
          "label": ""
        }
      ]
    },
    {
      "id": "4",
      "field": "Số điện thoại",
      "content": [
        {
          "value": "0869754261",
          "name": "phone",
          "type": "string",
          "label": ""
        }
      ]
    },
    {
      "id": "5",
      "field": "Nghề nghiệp, chức danh",
      "content": [
        {
          "value": "Giao vien",
          "name": "job",
          "type": "string",
          "label": ""
        }
      ]
    },
    {
      "id": "6",
      "field": "Mối quan hệ với sinh viên",
      "content": [
        {
          "value": "Giao vien",
          "name": "relationship",
          "type": "string",
          "label": ""
        }
      ]
    }
  ],
  "tab-2": [
    {
      "id": "1",
      "field": "Họ và tên",
      "content": [
        {
          "value": "Nguyễn Thị Trinh",
          "name": "namePresentee",
          "type": "string",
          "label": ""
        }
      ]
    },
    {
      "id": "2",
      "field": "Địa chỉ",
      "content": [
        {
          "value": "767 trần hưng đạo, Phường Điện Ngọc, Thị xã Điện Bàn, Tỉnh Quảng Nam",
          "name": "addressPresentee",
          "type": "string",
          "label": ""
        }
      ]
    },
    {
      "id": "3",
      "field": "Email",
      "content": [
        {
          "value": "nguyenthitrinh0935418514@gmail.com",
          "name": "emailPresentee",
          "type": "string",
          "label": ""
        }
      ]
    },
    {
      "id": "4",
      "field": "Số điện thoại",
      "content": [
        {
          "value": "0869754261",
          "name": "phonePresentee",
          "type": "string",
          "label": ""
        }
      ]
    },
    {
      "id": "5",
      "field": "Trúng tuyển vào ngành",
      "content": [
        {
          "value": "Quản lí nhân sự",
          "name": "major",
          "type": "string",
          "label": ""
        }
      ]
    },
    {
      "id": "6",
      "field": "Trúng tuyển vào trường",
      "content": [
        {
          "value": "Đại học đông á",
          "name": "university",
          "type": "string",
          "label": ""
        }
      ]
    },
    {
      "id": "7",
      "field": "Phương thức trúng tuyển",
      "content": [
        {
          "value": "Xét học bạ.",
          "name": "method",
          "type": "string",
          "label": ""
        },
        {
          "value": "Ghi chú: Xét tổ hợp môn",
          "name": "note",
          "type": "string",
          "label": ""
        }
      ]
    },
    {
      "id": "8",
      "field": "Giấy báo trúng tuyển",
      "content": [
        {
          "value": "/images/banner.jpeg",
          "name": "admissionNotice",
          "type": "image",
          "label": ""
        }
      ]
    },
    {
      "id": "9",
      "field": "Giấy chứng nhận hộ nghèo hoặc giấy xác nhận hoàn cảnh đặc biệt khó khăn của UBND phường, xã",
      "content": [
        {
          "value": "/images/banner.jpeg",
          "name": "frontCMND",
          "type": "image",
          "label": "Mặt trước"
        },
        {
          "value": "/images/banner.jpeg",
          "name": "backCMND",
          "type": "image",
          "label": "Mặt sau"
        }
      ]
    },
    {
      "id": "10",
      "field": "Hoàn cảnh gia đình",
      "content": [
        {
          "value": "Không có ba, ở với mẹ, chị, bà ngoại và một dì bị bệnh tật. Mẹ nay đã 50 bị đau xương khớp đi lại khó khăn. Chị mới ra trường chưa có công việc ổn định. Mẹ lo cho cả gia đình nên việc học phí còn khó khăn",
          "name": "situation",
          "type": "string",
          "label": ""
        }
      ]
    },
    {
      "id": "11",
      "field": "Nỗ lực cá nhân",
      "content": [
        {
          "value": "Đã cố gắng nổ lực trong nhiều năm qua đạt tốt nghiệp cấp 3",
          "name": "effort",
          "type": "string",
          "label": ""
        }
      ]
    }
  ],
  "tab-3": [
    {
      "id": "1",
      "field": "Thông tin khác",
      "content": [
        {
          "value": "Đã cố gắng nổ lực trong nhiều năm qua đạt tốt nghiệp cấp 3",
          "name": "description",
          "type": "string",
          "label": ""
        }
      ]
    }
  ]
}
```

- Example: **Status 400**

```json
{
  "message": "Thông tin không hợp lệ"
}
```

- Example: **Status 401**

```json
{
  "message": "Unauthorized"
}
```

## Số lượng đăng ký 7 ngày gần nhất ( ngày hôm nay và 6 ngày trước đó)

- URL: **${domain}/cms/recent7days**
- Method: **GET**
- Response:

| Status | Response data | Example                      | Description                                                                                 |
| ------ | ------------- | ---------------------------- | ------------------------------------------------------------------------------------------- |
| 200    | Number Array  | [50, 90, 10, 70, 60, 80, 30] | Mỗi một phần tử trong array là tổng số lượng đăng ký của 1 ngày, sắp xếp giảm dần theo ngày |

## Số lượng đăng ký theo tỉnh thành phố ( lấy tất cả tỉnh thành có giá trị )

- URL: **${domain}/cms/cities**
- Method: **GET**
- Response:

| Status | Response data | Example | Description                                                                                     |
| ------ | ------------- | ------- | ----------------------------------------------------------------------------------------------- |
| 200    | Object        |         | Mỗi một phần tử trong array là tổng số lượng đăng ký của 1 tỉnh, sắp xếp tăng dần theo số lượng |

- Example:

```json
{
  "cities": ["Thành phố Hồ Chí Minh", "Tỉnh Đồng Nai"],
  "data": [30, 16]
}
```
